
var vm = new Vue({
    el: '#app',
    data: {
        dialogAddHost: false,
        dialogAddProxy: false,
        dialogNewRecording: false,
        onlyOnSavedHost: false,
        recording: false,
        hostsToMock: [],
        ngr: "",
        ngrokUrl: "ngrok Desactivado",
        ngrok: false,
        newHost: null,
        newProxy: {},
        proxyHeaders: [
            { text: 'Proxy host', value: 'Proxy host', align: "left" },
            { text: 'Proxy port', value: 'porxy port', align: "left" },
            { text: 'Status', value: 'Status', align: "left" },
            { text: 'Delete', value: 'Delete', align: "left" },

        ],
        filtHostHeaders: [
            { text: 'Host', value: 'Host', align: "left" },
            { text: 'Record Request', value: 'Record Request', align: "left" },
            { text: 'Actions', value: 'Actions', align: "left" },


        ],
        proxys: [],
        filteredHosts: [],
    }, created: function () {
        axios.get("/proxy/list")
            .then(response => {
                this.proxys = response.data
            })
            .catch(e => {
                console.log(e);
                this.errors.push(e)
            });

        axios.get("/host/list")
            .then(response => {
                this.filteredHosts = response.data
            })
            .catch(e => {
                console.log(e);
                this.errors.push(e)
            })


    }, methods: {
        saveHost: function () {
            var nHost = {};
            nHost.host = this.newHost;
            nHost.active = false;
            console.log("--------------")
            console.log(nHost.host)
            console.log("--------------")
            this.filteredHosts.push(nHost);
            console.log("New host: " + this.newHost);
            //  TODO: REQUEST TO THE SERVER TO ADD THE DATA
            axios.post('/host', nHost).then(response => {
                console.log("respuesta Obtenida: " + response.data)
            })
                .catch(e => {
                    console.log(e)
                })
        }, startRecording: function () {

        },

        saveProxy: function () {

            this.proxys.push(this.newProxy);
            console.log("New proxy: " + this.newProxy);

            axios.post('/proxy', this.newProxy).then(response => {


                console.log("respuesta Obtenida: " + response.data)

                this.newProxy = {};
            })
                .catch(e => {
                    this.ngrokUrl = "No se pudo generar la url";
                    this.errors.push(e)
                })

        },
        toggleProxy: function (currentProxy) {
            console.log("data---_>" + currentProxy.useMe)
            if (!currentProxy.useMe) {
                for (i = 0; i < this.proxys.length; i++) {
                    this.proxys[i].useMe = false;
                }
                currentProxy.useMe = true;


            } else {
                currentProxy.useMe = false;
            }


            axios.put('/proxy', currentProxy).then(response => {


                console.log("respuesta Obtenida: " + response.data)

                this.newProxy = {};
            })
                .catch(e => {
                    console.error("No se ha podido actualizar el estado de este proxy");
                    if (currentProxy.useme) {
                        currentProxy.useme = false;
                    } else {
                        currentProxy.useme = true;
                    }

                })
        }, toggleHostToMock: function (toMock) {

            console.log(toMock);
            axios.put('/host', toMock).then(response => {


                console.log("respuesta Obtenida: " + response.data)

            })
                .catch(e => {
                    console.error("No se ha podido actualizar el estado de este proxy");
                    if (currentProxy.useme) {
                        currentProxy.useme = false;
                    } else {
                        currentProxy.useme = true;
                    }

                })



        },
        toggleNgrok: function () {
            if (this.ngrok) {
                this.ngrokUrl = "Generando url de ngrok..."
                axios.get('/ngrok')
                    .then(response => {

                        console.log("respuesta Obtenida: " + response.data)
                        this.ngrokUrl = response.data
                    })
                    .catch(e => {
                        this.ngrokUrl = "No se pudo generar la url";
                        this.errors.push(e)
                    })
            } else {
                this.ngrokUrl = "ngrok Desactivado";
                axios.delete('/ngrok')
                    .then(response => {

                        console.log("respuesta Obtenida: " + response.data)
                        this.ngrokUrl = response.data
                    })
                    .catch(e => {
                        this.errors.push(e)
                    })
            }

        }, startRecording: function () {
            this.recording = true;
        }
        , stopRecording: function () {
            this.recording = false;
        }

    }

})