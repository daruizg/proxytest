var express = require('express');
http = require('http');
path = require('path');
app = express();
fs = require('fs');

var bodyParser = require('body-parser');
app.use(bodyParser.json());

var mongoose = require('mongoose');


var promise = mongoose.connect('mongodb://localhost/proxy_mocker', {
    useMongoClient: true,
    /* other options */
});


var serverPort = 8765;
router = express.Router();
var ngrokConfigFile = "/tmp/ngrok_config.yml";
var ngrokProxyConf = "http_proxy:";

var ngrok = require('ngrok');
ngrok.disconnect();


proxyRouter = require("./routes/proxy");
hostRouter = require("./routes/host");


app.use(express.static(path.join(__dirname, 'public')));

app.use("/proxy", proxyRouter);
app.use("/host", hostRouter);

app.get("/ngrok", function (req, res) {


    var ngrConn = {
        addr: serverPort,
        configPath: ngrokConfigFile,
    }

    var fs = require('fs');


    fs.unlink(ngrokConfigFile, function (err) {
        if (err) console.log(err);
        console.log('file deleted successfully');
        fs.writeFile(ngrokConfigFile, ngrokProxyConf + ' "http://proxy.vtr.cl:8080"', { flag: 'wx' }, function (err) {
            if (err) {                
                console.log(err);
                return err;
            }

            console.log("The file was saved!");
            console.log("starting ngrok")
            ngrok.connect(ngrConn, function (err, url) {

                if (err) res.send("{error:'No se pudo crear la url'}");

                console.log("URL" + url);
                res.send(url);
            });
        });
    });

})
app.delete("/ngrok", function (req, res) {
    ngrok.disconnect();
})

app.get("/", function (req, res) {
    res.sendFile(__dirname + '/views/index.html');

})

app.use("/public",express.static('public'));

app.use("*", function (req, res) {

    res.send("NOTTTTTTTTTTTTTTTTTTTTT");
});





var server = app.listen(serverPort, function () {
    var host = server.address().address
    var port = server.address().port
    console.log("Example app listening at http://%s:%s", host, port)

})
