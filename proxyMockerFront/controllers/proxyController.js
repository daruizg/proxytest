var Proxy = require("../models/proxy");


module.exports.proxy_list = function (req, res) {
    console.log("getting proxies")
    Proxy.find({}, function (err, proxies) {

        if (err) {
            console.log(err);
            res.status(500).send({ error: 'No se pudo cargar los proxies !' });;
        }

        res.send(proxies);
    });
};

module.exports.proxy_get_current = function (req, res) {

    res.send('NOT IMPLEMENTED: proxy list');
};


module.exports.proxy_create = function (req, res) {
    console.log(req.body);

    var np = new Proxy({
        host: req.body.host,
        port: req.body.port,
        useMe: req.body.useme
    })

    np.save(function (err) {
        if (err) {
            console.log(err);
            res.status(500).send({ error: 'No se pudo guardar en la base de datos !' });
        }


        console.log('Data saved successfully!');
    });
    res.status(200).send({ msg: 'Usuario Guardado exitosamente' });
};

module.exports.proxy_update = function (req, res) {
    var pr = req.body;

    console.log(pr)

    if (pr.useMe) {
        Proxy.update({ useMe: true }, { useMe: false }, { multi: true },
            function (err, num) {
                console.log("Setting all to false " + num);
            }
        );

    }

    Proxy.update({ _id: pr._id }, { useMe: pr.useMe },
        function (err, num) {
            console.log("updating one " + num);
        }
    );



    res.status(200).send({ msg: 'Proxy actualizado exitosamente' });
};

module.exports.proxy_delete = function (req, res) {
    res.send('NOT IMPLEMENTED: proxy list');
};

