var express = require('express');
var router = express.Router();

var hostController=require("../controllers/hostController");


router.get("/list",hostController.host_getall);
router.post("/", hostController.host_create);
router.put("/", hostController.host_update);
router.delete("/",hostController.host_delete);

module.exports = router;