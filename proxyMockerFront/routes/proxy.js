var express = require('express');
var router = express.Router();

var proxyController=require("../controllers/proxyController");


router.get("/",proxyController.proxy_get_current);
router.get("/list", proxyController.proxy_list);
router.post("/", proxyController.proxy_create);
router.put("/", proxyController.proxy_update);
router.delete("/",proxyController.proxy_delete);

module.exports = router;