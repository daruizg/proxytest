var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var recSchema = new Schema({
  name: { type: String, required: true, unique: false },
  active: Boolean
});


var Recording = mongoose.model('Record', recSchema);


module.exports = Recording;