// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var proxySchema = new Schema({
  host: { type: String, required: true, unique: false },
  port: { type: String, required: true, unique: false },
  useMe: Boolean,

});

// the schema is useless so far
// we need to create a model using it
var Proxy = mongoose.model('Proxy', proxySchema);

// make this available to our users in our Node applications
module.exports = Proxy;