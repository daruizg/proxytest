var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var responseSch = new Schema({
  Host: { type: Schema.Types.ObjectId, ref: 'Host' },
  url: { type: String, required: true, unique: false },
  recording: { type: Schema.Types.ObjectId, ref: 'Recording' },
  response: { type: String, required: true, unique: false },
});


var Response = mongoose.model('Response', responseSch);


module.exports = Response;