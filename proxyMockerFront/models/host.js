var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var proxySchema = new Schema({
  host: { type: String, required: true, unique: false },
  active: Boolean
});


var Host = mongoose.model('Host', proxySchema);


module.exports = Host;