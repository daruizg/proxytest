package com.dr.proxymocker.pojos;

public class ProxyConf {
	
	public String host;
	public int port;
	public boolean useMe;
	
	
	public boolean isUseMe() {
		return useMe;
	}
	public void setUseMe(boolean useMe) {
		this.useMe = useMe;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	
	

}
