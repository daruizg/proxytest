package com.dr.proxymocker.managers;

import java.util.List;

import org.bson.Document;

import com.dr.proxymocker.pojos.ProxyConf;
import com.google.gson.Gson;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

public class DataManager {
	
	private MongoDatabase db= null;
	public DataManager(MongoDatabase db){
		this.db=db;
	
	}

	public String getStoredData(String url, String method) {
		String data = null;

		if (url == "buscar en la base de datos y ver si hay datos en el cache para este servicio") {
			data = "data recuperada desde la base de datos";
		}

		return data;

	}

	public Boolean saveData(String url, byte[] data) {
		
		String doc2 = new String(data);
		// Almacenar la información de la respuesta de esta url en la base de
		// datos.

		return true;

	}

	public List<String> getCacheableHost() {

		return null;

	}
	
	public Boolean isCacheableHost(String host){
		
		List<String> listaHost=getCacheableHost();
		if(listaHost==null)
			return false;
		
		if(listaHost.indexOf(host)!=-1)
			return true;
		
		return false;
		
		
	}
	
	public ProxyConf getProxy(){
		
		MongoCollection<Document> table = db.getCollection("proxys");
		Gson gson=new Gson();
		
		ProxyConf prx=null;
		
//		ProxyConf toInsert=new ProxyConf();
//		toInsert.setHost("proxy.vtr.cl");
//		toInsert.setPort(8080);
//		toInsert.setUseMe(true);
//		Document doc=new Document();
//		doc.parse(gson.toJson(toInsert));
//		
//		table.insertOne(doc);
		
		
		FindIterable<Document> find=table.find(Filters.eq("useMe", true)).limit(1);
		MongoCursor<Document> cursor = find.iterator();
		
		System.out.println(find);
		
		while (cursor.hasNext()) {
						
			prx =gson.fromJson(cursor.next().toJson(), ProxyConf.class);
		}	
		
		System.out.println("Recuperado de la bd-> "+prx);
		return prx;
	}

}
